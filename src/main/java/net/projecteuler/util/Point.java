package net.projecteuler.util;

public class Point {

    private Fraction x;
    private Fraction y;

    public Point(Fraction x, Fraction y) {
        this.x = x;
        this.y = y;
    }

    public Fraction getX() {
        return x;
    }

    public void setX(Fraction x) {
        this.x = x;
    }

    public Fraction getY() {
        return y;
    }

    public void setY(Fraction y) {
        this.y = y;
    }

}
