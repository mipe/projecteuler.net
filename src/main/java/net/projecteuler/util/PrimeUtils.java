package net.projecteuler.util;

import java.math.BigInteger;
import java.util.LinkedList;
import java.util.List;

public class PrimeUtils {

    public static List<Long> primes = new LinkedList<Long>();

    public static boolean brutePrime(long number) {
        long max = (long) Math.sqrt(number);
        for (Long prime : primes) {
            if (number % prime == 0) {
                return false;
            } else if (prime > max) {
                break;
            }
        }
        primes.add(number);
        return true;
    }

    public static BigInteger fac(int n) {
        BigInteger d = BigInteger.valueOf(1);
        for (int i = 1; i < n + 1; i++) {
            d = d.multiply(BigInteger.valueOf(i));
        }
        return d;
    }
}
