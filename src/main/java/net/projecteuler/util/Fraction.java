package net.projecteuler.util;

import java.math.BigInteger;

public class Fraction {

    private final BigInteger numerator;
    private final BigInteger denominator;

    public Fraction(long numerator, long denominator) {
        this.numerator = BigInteger.valueOf(numerator);
        this.denominator = BigInteger.valueOf(denominator);
    }

    public BigInteger getNumerator() {
        return numerator;
    }

    public BigInteger getDenominator() {
        return denominator;
    }

}