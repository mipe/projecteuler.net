package net.projecteuler.problems;

/**
 * Hello world!
 * 
 */
public class Problem0004 {

    public static final int MAX = 998001;

    public static void main(String[] args) {

        int maxI = 0;
        int maxJ = 0;
        int maxPalindrome = 0;

        for (int i = 100; i < 999; i++) {
            for (int j = 100; j < 999; j++) {
                int prod = i * j;
                if (isPalindrome(prod)) {
                    System.out.println(i + " " + j + " " + prod);
                    if (prod > maxPalindrome) {
                        maxPalindrome = prod;
                        maxI = i;
                        maxJ = j;
                    }
                }
            }
        }
        System.out.println(maxI + " " + maxJ + " " + maxPalindrome);
    }

    public static boolean isPalindrome(int number) {
        char[] s = new String(Integer.toString(number)).toCharArray();
        final int maxIndex = s.length - 1;
        for (int i = 0; i < maxIndex; i++) {
            if (i > maxIndex - i) {
                break;
            } else if (s[i] != s[maxIndex - i]) {
                return false;
            }
        }
        return true;
    }
}
