package net.projecteuler.problems;

import java.util.LinkedList;
import java.util.List;

public class Problem0007 {

    public static final int MAX = 10001;

    public static List<Long> primes = new LinkedList<Long>();

    public static void main(String[] args) {
        primes.add(2L);
        System.out.println(Math.sqrt(MAX));
        for (long i = 3L; primes.size() < MAX; i++) {
            if (brutePrime(i)) {
                // System.out.println("new prime " + i);
            }
            System.out.println(primes.get(primes.size() - 1) + " " + primes.size());
        }
        System.out.println(primes.get(primes.size() - 1));
    }

    public static boolean brutePrime(long number) {
        long max = (long) Math.sqrt(number);
        for (Long prime : primes) {
            if (number % prime == 0) {
                return false;
            } else if (prime > max) {
                break;
            }
        }
        primes.add(number);
        return true;
    }
}
