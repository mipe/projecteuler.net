package net.projecteuler.problems;

public class Problem0005 {

    public static final int MAX = 20;

    public static void main(String[] args) {
        for (int i = MAX;; i += MAX) {
            int j;
            for (j = 2; j < MAX + 1; j++) {
                if (i % j != 0) {
                    break;
                }
            }
            if (j > MAX) {
                System.out.println(i);
                break;
            }
        }
    }
}
