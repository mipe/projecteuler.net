package net.projecteuler.problems;

/**
 * Hello world!
 * 
 */
public class Problem0001 {

    public static final int MAX = 1000;

    public static void main(String[] args) {
        int a = 3;
        int b = 5;
        int lcm = 3 * 5;

        int da = d(a);
        int db = d(b);
        int dlcm = d(lcm);

        int ga = gauss(da) * a;
        int gb = gauss(db) * b;
        int glcm = gauss(dlcm) * lcm;

        int result = ga + gb - glcm;
        System.out.println(result);
    }

    public static int gauss(int number) {
        return (number * number + number) / 2;
    }

    public static int d(int number) {
        return MAX % number == 0 ? (MAX / number) - 1 : MAX / number;
    }
}
