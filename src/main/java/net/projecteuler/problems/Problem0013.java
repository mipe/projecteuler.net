package net.projecteuler.problems;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Problem0013 {

    public static void main(String[] args) throws FileNotFoundException {
        double sum = 0;
        try (Scanner scanner = new Scanner(new File("src/main/resources/Problem0013.txt"))) {
            while (scanner.hasNext()) {
                sum += Double.parseDouble(scanner.next());
            }
        }
        System.out.println(sum);
    }
}
