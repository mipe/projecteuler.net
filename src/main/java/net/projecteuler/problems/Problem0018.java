package net.projecteuler.problems;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Problem0018 {

    public static final int LOOKAHEAD = 4;

    public static void main(String[] args) throws FileNotFoundException {
        List<List<Integer>> list = new ArrayList<List<Integer>>();
        try (Scanner scanner = new Scanner(new File("src/main/resources/Problem0018.txt"))) {
            while (scanner.hasNext()) {
                List<Integer> row = new ArrayList<Integer>();
                for (String element : scanner.next().split(" ")) {
                    row.add(Integer.valueOf(element));
                }
            }
        }
        // int sum = 0;
        // int prevIndex = 0;
        // if (elements.length == 1) {
        // sum += Integer.valueOf(elements[0]);
        // } else {
        // int left = Integer.valueOf(elements[prevIndex]);
        // int right = Integer.valueOf(elements[prevIndex + 1]);
        // if (left > right) {
        // sum += left;
        // } else {
        // sum += right;
        // prevIndex++;
        // }
        //
        // }
        // }
        // System.out.println(sum);
        // }
    }
}
