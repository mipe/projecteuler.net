package net.projecteuler.problems;

import net.projecteuler.util.PrimeUtils;

/**
 * Hello world!
 * 
 */
public class Problem0003 {

    public static final long MAX = 600851475143L;

    public static void main(String[] args) {
        long largest = 1L;
        PrimeUtils.brutePrime(2L);
        for (long i = 3L; i < (MAX / 2) + 1; i += 2) {
            if (PrimeUtils.brutePrime(i)) {
                if (MAX % i == 0) {
                    largest = i;
                    System.out.println(largest);
                }
            }
        }
        System.out.println(largest);
    }
}
