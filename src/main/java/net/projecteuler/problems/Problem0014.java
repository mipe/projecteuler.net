package net.projecteuler.problems;


public class Problem0014 {

    public static final int MAX = 1000000;

    public static void main(String[] args) {
        long maxChain = 0;
        long maxVal = 0;
        for (long i = 1; i < MAX; i++) {
            long chain = collatz(i, 0);
            if (chain > maxChain) {
                maxChain = chain;
                maxVal = i;
            }
        }
        System.out.println(maxChain);
        System.out.println(maxVal);
    }

    private static long collatz(long n, long length) {
        if (n == 1)
            return length;
        length++;

        if (n % 2 == 0) {
            return collatz(n / 2, length);
        } else {
            return collatz((n * 3) + 1, length);
        }
    }
}
