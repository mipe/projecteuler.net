package net.projecteuler.problems;

public class Problem0017 {

    public static void main(String[] args) {
        long sum = 0;
        for (int i = 1; i < 1001; i++) {
            sum += getWord(i).length();
        }
        System.out.println(sum);
    }

    private static String getWord(int i) {
        if (i == 1000) {
            return "onethousand";
        }
        String s = "";
        int h = i / 100;
        int t = i % 100;
        if (h != 0) {
            s += getLessTen(h) + "hundred";

            if (t != 0) {
                s += "And" + getTen(t);
            }

        } else {
            if (t != 0) {
                s += getTen(t);
            } else {
                return "zero";
            }
        }
        return s;
    }

    private static String getTen(int i) {
        String s = "";
        if (i < 20) {
            if (i < 10) {
                return getLessTen(i);

            } else {
                if (i == 10)
                    return "ten";
                if (i == 11)
                    return "eleven";
                if (i == 12)
                    return "twelve";
                if (i == 13)
                    return "thirteen";
                if (i == 14)
                    return "fourteen";
                if (i == 15)
                    return "fifteen";
                if (i == 16)
                    return "sixteen";
                if (i == 17)
                    return "seventenn";
                if (i == 18)
                    return "eighteen";
                if (i == 19)
                    return "nineteen";
            }

        } else if (i >= 20 && i < 100) {
            int t = i / 10;
            int o = i % 10;
            if (o != 0) {
                s = getLessTen(o);
            }

            if (t == 2) {
                return "twenty" + s;
            }
            if (t == 3) {
                return "thirty" + s;
            }
            if (t == 4) {
                return "forty" + s;
            }
            if (t == 5) {
                return "fifty" + s;
            }
            if (t == 6) {
                return "sixty" + s;
            }
            if (t == 7) {
                return "seventy" + s;
            }
            if (t == 8) {
                return "eighty" + s;
            }
            if (t == 9) {
                return "ninety" + s;
            }
        }
        return null;
    }

    private static String getLessTen(int i) {
        if (i == 0)
            return "zero";
        if (i == 1)
            return "one";
        if (i == 2)
            return "two";
        if (i == 3)
            return "three";
        if (i == 4)
            return "four";
        if (i == 5)
            return "five";
        if (i == 6)
            return "six";
        if (i == 7)
            return "seven";
        if (i == 8)
            return "eight";
        if (i == 9)
            return "nine";
        throw new IllegalArgumentException();

    }
}
