package net.projecteuler.problems;

public class Problem0012 {

    public static final int MAX = 500;

    public static void main(String[] args) {
        int sum = 0;
        for (int i = 1;; i++) {
            sum += i;
            if (sum % 2 == 0) {
                if (getDivisors(sum) > 500) {
                    break;
                }
            }
        }
        System.out.println(sum);
    }

    public static int getDivisors(int number) {
        int divs = 3;
        for (int i = 3; i < (number / 2) + 1; i++) {
            if (number % i == 0)
                divs++;
        }
        System.out.println(number + " " + divs);
        return divs;
    }
}
