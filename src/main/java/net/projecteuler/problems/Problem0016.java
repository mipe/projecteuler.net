package net.projecteuler.problems;

import java.math.BigInteger;
import java.text.DecimalFormat;

public class Problem0016 {

    public static void main(String[] args) {
        BigInteger d = BigInteger.valueOf(2).pow(1000);
        DecimalFormat df = new DecimalFormat();
        int sum = 0;
        for (char c : d.toString().toCharArray()) {
            sum += Integer.valueOf(String.valueOf(c));
        }
        System.out.println(sum);
    }
}
