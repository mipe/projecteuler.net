package net.projecteuler.problems;

public class Problem0009 {

    public static void main(String[] args) {
        for (int a = 1; a < 995; a++) {
            for (int b = 2; b < 996; b++) {
                for (int c = 3; c < 997; c++) {
                    if (a < b && b < c) {
                        if ((a + b + c) == 1000) {
                            if (Math.pow(a, 2) + Math.pow(b, 2) == Math.pow(c, 2)) {
                                System.out.println(a * b * c);
                            }
                        }
                    }
                }
            }
        }
    }
}
