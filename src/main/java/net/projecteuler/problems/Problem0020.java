package net.projecteuler.problems;

import java.math.BigInteger;

public class Problem0020 {

    public static final int MAX = 100;

    public static void main(String[] args) {
        BigInteger d = BigInteger.valueOf(1);
        for (int i = 1; i < MAX + 1; i++) {
            d = d.multiply(BigInteger.valueOf(i));
        }
        System.out.println(d);
        long sum = 0;
        for (char c : d.toString().toCharArray()) {
            sum += Integer.valueOf(String.valueOf(c));
        }
        System.out.println(sum);
    }
}
