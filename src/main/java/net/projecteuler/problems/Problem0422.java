package net.projecteuler.problems;

import net.projecteuler.util.Fraction;
import net.projecteuler.util.Point;

public class Problem0422 {

    public static final Point X = new Point(new Fraction(7, 1), new Fraction(1, 1));
    public static final Point P1 = new Point(new Fraction(13, 1), new Fraction(61, 4));
    public static final Point P2 = new Point(new Fraction(-43, 6), new Fraction(-4, 1));
    public static final Point P3 = new Point(new Fraction(-19, 2), new Fraction(-229, 24));
    public static final Point P4 = new Point(new Fraction(1267, 144), new Fraction(-37, 12));
    public static final Point P7 = new Point(new Fraction(17194218091L, 143327232), new Fraction(274748766781L,
            1719926784));

    public static void main(String[] args) {

    }
}
