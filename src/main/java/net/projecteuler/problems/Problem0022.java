package net.projecteuler.problems;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Problem0022 {

    public static final char ALPHABET[] = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O',
            'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };

    public static void main(String[] args) throws FileNotFoundException {
        Map<Character, Integer> map = new HashMap<Character, Integer>();
        for (int i = 0; i < ALPHABET.length; i++) {
            map.put(ALPHABET[i], i + 1);
        }

        try (Scanner scanner = new Scanner(new File("src/main/resources/Problem0022.txt"))) {
            String s = scanner.next();
            String[] names = s.replaceAll("\"", "").split(",");
            Arrays.sort(names);
            int sum = 0;
            for (int i = 0; i < names.length; i++) {
                int nameSum = 0;
                for (char c : names[i].toCharArray()) {
                    nameSum += map.get(c);
                }
                sum += nameSum * (i + 1);
            }
            System.out.println(sum);
        }
    }
}
