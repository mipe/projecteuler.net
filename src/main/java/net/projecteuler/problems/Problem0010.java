package net.projecteuler.problems;

import net.projecteuler.util.PrimeUtils;

public class Problem0010 {

    public static final long MAX = 2000000L;

    public static void main(String[] args) {
        long sum = 0L;
        for (long i = 2L; i < MAX; i++) {
            if (PrimeUtils.brutePrime(i)) {
                sum += i;
            }
        }
        System.out.println(sum);
    }
}
