package net.projecteuler.problems;

public class Problem0006 {

    public static final int MAX = 100;

    public static void main(String[] args) {
        // long sumDiff = 0;
        // long sumSquare = 0;
        // long sum = 0;
        // long squareSum = 0;
        // for (int i = 1; i < MAX; i++) {
        // sumSquare += Math.pow(i, 2);
        // sum += i;
        // squareSum = (long) Math.pow(sum, 2);
        // sumDiff += squareSum - sumSquare;
        // }

        long sumOfSquares = MAX * (MAX + 1) * ((2 * MAX) + 1) / 6;
        long squareOfSums = (long) Math.pow(MAX * (MAX + 1) / 2, 2);
        System.out.println(squareOfSums - sumOfSquares);
    }
}
