package net.projecteuler.problems;

import java.math.BigInteger;

import net.projecteuler.util.PrimeUtils;

public class Problem0015 {

    public static final int MAX = 20;

    public static void main(String[] args) {
        System.out.println(bin(MAX * 2, MAX));
    }

    public static BigInteger bin(int n, int k) {
        return PrimeUtils.fac(n).divide(PrimeUtils.fac(k).multiply(PrimeUtils.fac(n - k)));
    }
}
