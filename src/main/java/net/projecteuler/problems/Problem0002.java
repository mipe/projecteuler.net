package net.projecteuler.problems;

/**
 * Hello world!
 * 
 */
public class Problem0002 {

    public static final int MAX = 4000000;

    public static void main(String[] args) {
        int n0 = 1;
        int n1 = 1;
        int sum = 0;
        fib(n0, n1, sum);
    }

    public static void fib(int n0, int n1, int sum) {
        if (n1 >= MAX) {
            System.out.println(sum);
            return;
        }
        int n2 = n0 + n1;
        if (n2 % 2 == 0) {
            sum += n2;
        }
        fib(n1, n2, sum);
    }
}
