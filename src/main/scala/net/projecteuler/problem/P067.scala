package net.projecteuler.problem

import net.projecteuler.util.NodeTreeBuilder
import net.projecteuler.util.Node

object P067 {
  var best = 0

  def main(args: Array[String]) {
    val filepath = "src/main/resources/triangle.txt" //1074
    //    val filepath = "src/main/resources/test.txt" //23
    //    print(root.value + greedySum(root.nextLeft,root.nextRight))
    val builder = new NodeTreeBuilder()
    depthFirst(builder.buildTree(filepath), 0)
    print(best)
  }

  def depthFirst(node: Node, sum: Int) {
    if (node == null) {
      return
    }
    val cur = sum + node.value
    if (cur > node.maxValue) {
      node.maxValue = cur
      if (cur > best) {
        best = cur
      }
    } else {
      return
    }
    depthFirst(node.nextLeft, cur)
    depthFirst(node.nextRight, cur)
  }
}