package net.projecteuler.problem

object P019 {
  var days = 0
  var sundays = 0

  def main(args: Array[String]) {
    println(processYear(1900))
    for (i <- 1901 to 2000) {
      sundays += processYear(i)
    }
    println(sundays)
  }

  def processYear(year: Int): Int = {
    var count = 0
    days += 31 //Jan
    count += isSunday
    days += 28 //Feb
    if (year % 4 == 0) {
      if (year % 100 == 0) {
        if (year % 400 == 0) {
          days += 1
        }
      } else {
        days += 1
      }
    }
    count += isSunday
    days += 31 //Mar
    count += isSunday
    days += 30
    count += isSunday
    days += 31 //May
    count += isSunday
    days += 30
    count += isSunday
    days += 31
    count += isSunday
    days += 31
    count += isSunday
    days += 30 //Sep
    count += isSunday
    days += 31
    count += isSunday
    days += 30
    count += isSunday
    days += 31 //Dec
    count += isSunday
    return count
  }

  def isSunday(): Int = {
    return if (days % 7 == 6) 1 else 0
  }
}