package net.projecteuler.problem

import scala.io.Source
import scala.collection.mutable.ListBuffer
import net.projecteuler.util.Node
import net.projecteuler.util.NodeTreeBuilder

object P018 {
  def main(args: Array[String]) {
    val filepath = "src/main/resources/Problem0018.txt" //1074
    //    val filepath = "src/main/resources/test.txt" //23
    //    print(root.value + greedySum(root.nextLeft,root.nextRight))
      val builder = new NodeTreeBuilder() 
    print(subMax(builder.buildTree(filepath)))
  }

  def greedySum(left: Node, right: Node): Int = {
    if (left == null || right == null) {
      return 0
    }
    if (left.value > right.value) {
      return left.value + greedySum(left.nextLeft, left.nextRight)
    } else {
      return right.value + greedySum(right.nextLeft, right.nextRight)
    }
  }

  def subMax(node: Node): Int = {
    if (node.nextLeft == null || node.nextRight == null) {
      return node.value
    }
    val leftMax = subMax(node.nextLeft)
    val rightMax = subMax(node.nextRight)
    if (leftMax > rightMax) {
      return node.value + leftMax
    } else {
      return node.value + rightMax
    }
  }
}