package net.projecteuler.util

import scala.io.Source
import scala.collection.mutable.ListBuffer

class NodeTreeBuilder {
  def buildTree(filepath: String): Node = {
    val source = Source.fromFile(filepath)
    var nodeList = new ListBuffer[Node]
    for (line <- source.getLines) {
      line.split(" ").foreach(value => {
        val n = new Node
        nodeList += n
        n.id = nodeList.size
        n.value = value.toInt
      })
    }
    val root = nodeList(0)
    linkNodes(root, nodeList(1), nodeList(2))
    def buildTree(i: Int, max: Int) {
      var current = nodeList(i)
      if (current.prevRight != null) {
        val nextLeft = current.id + (current.id - current.prevRight.id)
        if (nextLeft >= nodeList.size) {
          return
        }
        val nextRight = nextLeft + 1
        linkNodes(current, nodeList(nextLeft), nodeList(nextRight))
      } else if (current.prevLeft != null) {
        val nextRight = current.id + (current.id - current.prevLeft.id)
        if (nextRight >= nodeList.size) {
          return
        }
        val nextLeft = nextRight - 1
        linkNodes(current, nodeList(nextLeft), nodeList(nextRight))
      }
      if (i < nodeList.size) {
        buildTree(i + 1, nodeList.size)
      }
    }
    buildTree(1, nodeList.size)
    source.close
    return root
  }

  def linkNodes(parent: Node, left: Node, right: Node) {
    parent.nextLeft = left
    left.prevRight = parent
    parent.nextRight = right
    right.prevLeft = parent
  }
}

object NodeTreeBuilder extends NodeTreeBuilder {
}
