package net.projecteuler.util

class Node {
  var id: Int = 0
  var value: Int = 0
  var maxValue: Int = 0
  var prevLeft: Node = null
  var prevRight: Node = null
  var nextLeft: Node = null
  var nextRight: Node = null

  //  def toString() {
  //    
  //  }

}

object Node extends Node {
}